//
//  ChecklistItem.swift
//  CheckListUIKit
//
//  Created by Victor Tejada Yau on 5/17/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import Foundation

class ChecklistItem: NSObject {
    
    @objc var text = ""
    var checked = false
    
    func toggleChecked() {
        checked = !checked
    }

}
