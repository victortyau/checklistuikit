//
//  TodoList.swift
//  CheckListUIKit
//
//  Created by Victor Tejada Yau on 5/18/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import Foundation

class TodoList {
    
    enum Priority: Int, CaseIterable {
        case high, medium, low, no
    }
    
    private var highPriorityTodos: [ChecklistItem] = []
    private var mediumPriorityTodos: [ChecklistItem] = []
    private var lowPriorityTodos: [ChecklistItem] = []
    private var noPriorityTodos: [ChecklistItem] = []
    
    init() {
        let row0Item = ChecklistItem()
        let row1Item = ChecklistItem()
        let row2Item = ChecklistItem()
        let row3Item = ChecklistItem()
        let row4Item = ChecklistItem()
        
        row0Item.text = "Fedora Project"
        row1Item.text = "CentOS Project"
        row2Item.text = "Red Hat Project"
        row3Item.text = "Suse Project"
        row4Item.text = "Ubuntu Project"
        
        addTodo(row0Item, for: .high)
        addTodo(row1Item, for: .medium)
        addTodo(row2Item, for: .low)
        addTodo(row3Item, for: .no)
        addTodo(row4Item, for: .no)
    }
    
    func newTodo() -> ChecklistItem {
        let item = ChecklistItem()
        item.text = randomTitle()
        item.checked =  true
        mediumPriorityTodos.append(item)
        return item
    }
    
    func move(item: ChecklistItem, from sourcePriority: Priority, at sourceIndex: Int, to destinationPriority: Priority, at destinationIndex: Int) {
        remove(item, from: sourcePriority, at: sourceIndex)
        addTodo(item, for: destinationPriority, at: destinationIndex)
        
    }
    
    func addTodo(_ item: ChecklistItem, for priority: Priority, at index: Int = -1) {
        switch priority {
          case .high:
            if index < 0 {
                highPriorityTodos.append(item)
            } else {
                highPriorityTodos.insert(item, at: index)
            }
             
          case .medium:
             if index < 0 {
                mediumPriorityTodos.append(item)
             } else {
                mediumPriorityTodos.insert(item, at: index)
             }
            
          case .low:
            if index < 0 {
               lowPriorityTodos.append(item)
            } else {
                lowPriorityTodos.insert(item, at: index)
            }
            
          case .no:
            if index < 0 {
                noPriorityTodos.append(item)
            } else {
                noPriorityTodos.insert(item, at: index)
            }
          }
    }
    
    func todoList(for priority: Priority) -> [ChecklistItem] {
        switch priority {
        case .high:
            return highPriorityTodos
        case .medium:
            return mediumPriorityTodos
        case .low:
            return lowPriorityTodos
        case .no:
            return noPriorityTodos
        }
    }
    
    func remove(_ item: ChecklistItem, from priority: Priority, at index: Int) {
        switch priority {
        case .high:
            highPriorityTodos.remove(at: index)
        case .medium:
            mediumPriorityTodos.remove(at: index)
        case .low:
            lowPriorityTodos.remove(at: index)
        case .no:
            noPriorityTodos.remove(at: index)
        }
    }
    
//    func remove(items: [ChecklistItem]) {
//        for item in items {
//            if let index = todos.firstIndex(of: item) {
//                todos.remove(at: index)
//            }
//        }
//    }
    
    private func randomTitle() -> String {
        let titles = ["Gentoo Linux", "Linux Mint", "Gobo Linux", "GNU Sense", "Ututo Linux"]
        let randomNumber = Int.random(in: 0 ... titles.count - 1)
        return titles[randomNumber]
    }
}
